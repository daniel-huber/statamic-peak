Im Tutorial wird der _Nginx_-Blueprint verwendet:

![](https://slabstatic.com/prod/uploads/gnkkzg3p/posts/images/C3BZrRIBA0W5GFNO28XfgQD9.png)

## Vorinstallierte Software

- nginx
- PHP (v7.4)
- composer
- git
- phpMyAdmin
- etc.

## Allgemeine Informationen

- Main-User: bitnami
- Web-User: daemon
- `daemon` muss der Owner der Web-Project Folders und allen darin liegenden Verzeichnissen und Dateien sein
- die bereits vorinstallierte Software befindet sich unter `/opt/bitnami`

# Installation

1. mittels SSH auf den Server verbinden
1. `sudo mkdir /var/www`
1. `sudo chown bitnami:bitnami /var/www`
1. `git clone https://gitlab.com/daniel-huber/statamic-peak.git /var/www/statamic-peak`
1. `cd /var/www/statamic-peak && composer install`
1. `sudo chown -R bitnami:bitnami /var/www/statamic-peak`
1. `sudo nano /opt/bitnami/nginx/conf/server_blocks/statamic-peak-server-block.conf`
1. Folgenden Inhalt einfügen und abspeichern:

```
server {
    # Port to listen on, can also be set in IP:PORT format
    listen 80 default_server;
    root /var/www/statamic-peak/public;
    # Catch-all server block
    # See: https://nginx.org/en/docs/http/server_names.html#miscellaneous_names
    server_name _;
    include  "/opt/bitnami/nginx/conf/bitnami/*.conf";
 
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
 
    index index.html index.htm index.php;
 
    charset utf-8;
 
    location / {
        try_files /static${uri}_${args}.html $uri $uri/ /index.php?$query_string;
    }
 
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
 
    error_page 404 /index.php;
 
    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
 
    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```

1. `sudo /opt/bitnami/ctlscript.sh restart`
1. Fertig 🚀

# Login

- URL: `/cp`
- User: `demo@dryven.at`
- Passwort: `superadmin`
